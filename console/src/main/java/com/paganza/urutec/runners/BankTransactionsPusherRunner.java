package com.paganza.urutec.runners;

import com.google.gson.Gson;
import com.paganza.urutec.handlers.ExceptionHandler;
import com.paganza.urutec.helpers.DateFormatterHelper;
import com.paganza.urutec.interfaces.IBlobStorageService;
import com.paganza.urutec.interfaces.ICCAExecutor;
import com.paganza.urutec.services.CommunicationPlatformService;
import com.paganza.urutec.services.TelemetryService;

import java.time.LocalDateTime;
import java.util.HashMap;

public abstract class BankTransactionsPusherRunner implements Runnable {
    private final Thread thread;
    protected final Gson gson;
    protected final IBlobStorageService blobStorageService;
    protected final ICCAExecutor iccaExecutor;
    protected final String blobContainerDestination;

    public BankTransactionsPusherRunner(IBlobStorageService blobStorageService, ICCAExecutor iccaExecutor, String blobContainerDestination) {
        this.blobStorageService = blobStorageService;
        this.iccaExecutor = iccaExecutor;
        this.blobContainerDestination = blobContainerDestination;
        this.thread = new Thread(this);
        this.gson = new Gson();
    }
//banktransactionspuller
    //banktransactionpusher ?___?
    @Override
    public final void run() {
        try {

            this.iccaExecutor.addExecution(iccaService -> {
                try {
                    track("PullTransactions Started", null);
                    var transactions = pullTransactions();
                    track("PullTransactions Finished", transactions);
                    processTransactions(transactions);
                    track("Process transactions Finished", transactions);
                    if (transactions != null && ((Object[]) transactions).length > 0)
                        pushTransactions(gson.toJson(transactions));
                    track(String.format("Transactions upload to Blob Storage following by this path: %s", this.blobContainerDestination), transactions);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception ex) {
            ExceptionHandler.TrackException(ex, null);
        }
    }
    public final void start() {
        this.thread.start();
    }

    protected String buildBlobName() {
        return this.blobContainerDestination + "Transactions-CashIn_"
                + DateFormatterHelper.dateFormat(LocalDateTime.now(), "yyyy-MM-dd_HH-mm-ss");
    }
    protected void pushTransactions(final String content) {
        try {
            this.blobStorageService.uploadBlob(buildBlobName(), content);
        } catch (Exception ex) {
            ExceptionHandler.TrackException(ex, content);
        }
    }
    protected void processTransactions(Object transactions) { }
    protected abstract Object pullTransactions();

    private void track(final String eventName, final Object object) {
        System.out.println(this.getClass().getName() + ": " + eventName);

        try {
            CommunicationPlatformService.Notify(this.getClass().getName() + ": " + eventName);
        } catch (Exception e) { }

        HashMap<String, String> map = null;
        if (object != null) {
            map = new HashMap<>();
            map.put("Object", this.gson.toJson(object));
        }
        TelemetryService.getInstance().trackEvent(eventName, map, null);
    }
}