package com.paganza.urutec.runners;

import com.paganza.urutec.handlers.ExceptionHandler;
import com.paganza.urutec.helpers.DateFormatterHelper;
import com.paganza.urutec.interfaces.IBlobStorageService;
import com.paganza.urutec.interfaces.ICCAExecutor;
import com.paganza.urutec.interfaces.ICCAService;
import com.paganza.urutec.services.CommunicationPlatformService;
import com.paganza.urutec.services.TelemetryService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;

import com.google.gson.Gson;
import mtbevsa.clienteMTBevsa.libreria.LibreriaException;


public abstract class BankTransactionsPullerRunner implements Runnable {
    private final Thread thread;
    protected final Gson gson;
    protected final IBlobStorageService blobStorageService;
    protected final ICCAExecutor iccaExecutor;
    protected final String blobContainerDestination;

    public BankTransactionsPullerRunner(IBlobStorageService blobStorageService, ICCAExecutor iccaExecutor, String blobContainerDestination) {
        this.blobStorageService = blobStorageService;
        this.iccaExecutor = iccaExecutor;
        this.blobContainerDestination = blobContainerDestination;
        this.thread = new Thread(this);
        this.gson = new Gson();
    }

    @Override
    public final void run() {
       this.iccaExecutor.addExecution(iccaService -> {
            track("PullTransactions Started", null);
            var transactions = pullTransactions(iccaService);
            track("PullTransactions Finished", transactions);
            if (transactions != null && ((Object[]) transactions).length > 0)
                pushTransactions(gson.toJson(transactions));
            track(String.format("Transactions upload to Blob Storage following by this path: %s", this.blobContainerDestination), transactions);
        });
    }
    public final void start() {
        this.thread.start();
    }

    protected void pushTransactions(final String content) {
        this.blobStorageService.uploadBlob(buildBlobName(), content);
    }
    protected abstract Object pullTransactions(ICCAService ccaService) throws LibreriaException, IOException;

    private String buildBlobName() {
        return this.blobContainerDestination + "Transactions-CashIn_"
                + DateFormatterHelper.dateFormat(LocalDateTime.now(), "yyyy-MM-dd_HH-mm-ss");
    }
    private void track(final String eventName, final Object object) {
        System.out.println(this.getClass().getName() + ": " + eventName);

        try {
            CommunicationPlatformService.Notify(this.getClass().getName() + ": " + eventName);
        } catch (Exception e) { }

        HashMap<String, String> map = null;
        if (object != null) {
            map = new HashMap<>();
            map.put("Object", this.gson.toJson(object));
        }
        TelemetryService.getInstance().trackEvent(eventName, map, null);
    }
}