package com.paganza.urutec.console;

import java.util.concurrent.TimeUnit;

public class Configurations {
    // CCA parameters
    public static final String CCA_CONFIGURATION_FILE = "src/main/resources/confMTBLibreria.xml";
    public static final String CCA_USER = "libreria";
    public static final String CCA_PASSWORD = "clave2010";
    public static  final int THREAD_NUMBERS = 2;
    public static final int CONNEXION_INNACTIVITE_TIME = 5;

    // BlobStorage parameters
    public static final String ENDPOINT_PRD = "undefined";
    public static final String ENDPOINT_DEV = "https://paganzadevel.blob.core.windows.net/?sv=2020-08-04&ss=bf&srt=sco&sp=rwdlacx&se=2022-01-01T02:57:08Z&st=2021-09-10T18:57:08Z&spr=https&sig=OWKnCX8NOl8qpYHeyZJjEa84PChMVFnpTQe7%2FkdB%2F68%3D";
    public static final String BLOB_CONTAINER_NAME = "urutec";

    // Slack paramters
    public static String WEBHOOK_URL = "https://hooks.slack.com/services/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";

    // Path folders
    public static final String PENDING_BLOB_CONTAINER_PATH = "pendings/";
    public static final String PENDING_CONCILIATION_BLOB_CONTAINER_PATH = "pending-conciliation/";
    public static final String PENDING_ACCREDITATION_BLOB_CONTAINER_PATH = "pending-accreditation/";
    public static final String PENDING_CANCELATION_BLOB_CONTAINER_PATH = "pending-cancelation/";
    public static final String SENDED_BLOB_CONTAINER_PATH = "sended/";
    public static final String CONCILIADAS_BLOB_CONTAINER_PATH = "pendings/";

    // Scheduler configuration
    public static final TimeUnit SCHEDULER_TIME_UNIT = TimeUnit.SECONDS;
    public static final int PENDING_BLOB_CONTAINER_INITIAL_DELAY = 10;
    public static final int PENDING_BLOB_CONTAINER_PERIOD = 120;
    public static final int PENDING_ACCREDITATION_BLOB_CONTAINER_INITIAL_DELAY = 10;
    public static final int PENDING_ACCREDITATION_BLOB_CONTAINER_PERIOD = 120;
}
