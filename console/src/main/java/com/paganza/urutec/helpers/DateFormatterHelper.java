package com.paganza.urutec.helpers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatterHelper {
    public static String dateFormat(final LocalDateTime localDateTime, final String pattern) {
        return DateTimeFormatter.ofPattern(pattern).format(localDateTime);
    }
    public static String libraryDateFormat(final LocalDateTime localDateTime) {
        return DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(localDateTime);
    }
    public static LocalDateTime dateAtStartOfDay() {
        return LocalDateTime.now().toLocalDate().atStartOfDay();
    }
}
