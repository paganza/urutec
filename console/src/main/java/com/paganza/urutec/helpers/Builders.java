package com.paganza.urutec.helpers;

import com.paganza.urutec.console.Constants;
import mtbevsa.bd.TransaccionBanco;
import mtbevsa.bd.TransaccionBancoAcreditadaCancelada;
import mtbevsa.clienteMTBevsa.libreria.Transaccion;
import mtbevsa.comMtBevsa.MTConstantes;
import mtbevsa.util.Fecha;

public class Builders {
    public static Transaccion Transaccion(final String currency, final String amount){
        var t = new Transaccion();
        t.setCuentaACuenta(true);
        t.setTipoLiquidacion(MTConstantes.TIPO_LIQUIDACION_REAL);
        t.setCodigoISOMoneda(currency);
        t.setMonto(amount);
        t.setCodigoConcepto("TDD");
        t.setBicOrigen(Constants.PAGANZA2_BIC);
        t.setBicDestino(Constants.PAGANZA_BIC);
        t.setCuentaOrigen("2");
        t.setCuentaDestino("1");
        t.setNombreOrigen("PAGANZA2");
        t.setNombreDestino("PAGANZA");
        t.setFechaALiquidar(new Fecha());
        t.setDirOrigen("Palmar 2626");
        t.setDirDestino("Luis Alberto de Herrera");
        t.setTextoLibre("Transaccion de prueba");
        t.setAcredita(true);
        return t;
    }

    public static TransaccionBancoAcreditadaCancelada BankTransactionAccreditedCanceled(TransaccionBanco tx) {
        var t = new TransaccionBancoAcreditadaCancelada();
        t.setIdUsuarioAcrCanPago("dummieValue");
        t.setCodigoInstitucionAcrCanPago("dummieValue");
        t.setNumeroAutorizacion("dummieValue");
        t.setMotivoCancelacion(null);
        t.setCodigoEstadoTransacAcrCan('A');
        t.setObservaciones("dummieValue");
        t.setMotivoCancelacion(null);
        t.setEstadoAcreditacionCancelacion(null);

        return t;
    }
}
