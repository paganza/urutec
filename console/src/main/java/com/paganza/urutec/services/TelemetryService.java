package com.paganza.urutec.services;

import java.util.Map;

import com.microsoft.applicationinsights.TelemetryClient;
import com.paganza.urutec.interfaces.ITelemetryService;

public class TelemetryService implements ITelemetryService {
    private static TelemetryClient telemetryClient;

    private TelemetryService() { }

    public static TelemetryClient getInstance() {
        if (telemetryClient == null)
            telemetryClient = new TelemetryClient();
        return telemetryClient;
    }

    public void trackEvent(String eventName, Map<String, String> properties) {
        telemetryClient.trackEvent(eventName, properties, null);
    }
    public void trackException(Exception exception, Map<String, String> properties) {
        telemetryClient.trackException(exception, properties, null);
    }
    public void trackMetric(String metricName, double value) {
        telemetryClient.trackMetric(metricName, value);
    }
}
