package com.paganza.urutec.services;

import com.paganza.urutec.console.Configurations;
import com.slack.api.Slack;
import java.io.IOException;

public class CommunicationPlatformService {
    public static void Notify(String msg) throws IOException {
        var payload = String.format("{\"text\":\"%s\"}", msg);
        Slack.getInstance().send(Configurations.WEBHOOK_URL, payload);
    }
}
