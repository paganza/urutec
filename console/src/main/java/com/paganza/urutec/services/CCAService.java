package com.paganza.urutec.services;

import com.paganza.urutec.console.Configurations;
import com.paganza.urutec.interfaces.ICCAService;

import com.paganza.urutec.interfaces.ICCAServiceConnexion;
import mtbevsa.bd.TransaccionBanco;
import mtbevsa.bd.TransaccionBancoAcreditadaCancelada;
import mtbevsa.clienteMTBevsa.libreria.Libreria;
import mtbevsa.clienteMTBevsa.libreria.LibreriaException;
import mtbevsa.clienteMTBevsa.libreria.Transaccion;
import mtbevsa.clienteMTBevsa.mensajeria.Status;
import mtbevsa.comMtBevsa.MTConstantes;
import mtbevsa.util.Fecha;

import java.io.IOException;

public class CCAService implements ICCAService, ICCAServiceConnexion {
    private final Libreria library;

    public CCAService() throws LibreriaException {
        this.library = new Libreria(Configurations.CCA_CONFIGURATION_FILE);
    }

    public void enableConnexion() throws LibreriaException {
        this.library.conectar(Configurations.CCA_USER, MTConstantes.MMTB_CANAL_ONLINE);
        this.library.logon(Configurations.CCA_PASSWORD);
    }
    public TransaccionBanco[] getTransactionsByHour(final String fromHour) throws LibreriaException, IOException {
        return this.library.consultarTransaccionesDestino(fromHour);
    }
    public TransaccionBanco[] getTransactionsByRangeDate(final String fromDate, final String toDate) throws LibreriaException, IOException {
        return this.library.consultarTransaccionesFecha(fromDate, toDate);
    }
    public TransaccionBanco[] consultarTransaccionesDestinoLiquidadas(final String fromHour) throws LibreriaException, IOException {
        return this.library.consultarTransaccionesDestinoLiquidadas(fromHour);
    }
    public TransaccionBancoAcreditadaCancelada[] getTransactionsByHourToAccreditOrCancel(final String fromHour) throws LibreriaException, IOException {
        return this.library.consultarTransaccionesAcreditadaCanceladaDestino(fromHour);
    }
    public TransaccionBancoAcreditadaCancelada[] getTransactionsAccreditedCancelatedLiquidadas(final String fromDate) throws LibreriaException, IOException {
        return this.library.consultarTransaccionesAcreditadasCanceladasLiquidadasReceptor(new Fecha(fromDate));
    }
    public TransaccionBancoAcreditadaCancelada[] novedades(final String orden, final char liquidationType, final String fromDate) throws LibreriaException, IOException {
        return this.library.consultarTransaccionesAcreditadasCanceladasCambioEstado(orden, liquidationType, new Fecha(fromDate));
    }
    public Status accreditTransaction(final TransaccionBancoAcreditadaCancelada[] transactions) throws LibreriaException, IOException {
        return this.library.acreditarTransacciones(transactions);
    }
    public Status cancelTransaction(final TransaccionBancoAcreditadaCancelada[] transactions) throws LibreriaException, IOException {
        return this.library.cancelarTransaccionesAAcreditar(transactions);
    }

    public Status sendTransactions(final Transaccion tx) throws LibreriaException, IOException {
        return this.library.ingresarTransaccion(tx);
    }
}
