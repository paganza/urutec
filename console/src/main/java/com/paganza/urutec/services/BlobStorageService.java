package com.paganza.urutec.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import com.azure.core.http.rest.PagedIterable;
import com.azure.storage.blob.*;
import com.azure.storage.blob.models.*;
import com.paganza.urutec.console.Configurations;
import com.paganza.urutec.handlers.ExceptionHandler;
import com.paganza.urutec.interfaces.IBlobStorageService;

public class BlobStorageService implements IBlobStorageService {
    public String getBlob(String blobName){
        var blobContainerClient = createBlobContainerClient();
        var blobClient = blobContainerClient.getBlobClient(blobName);

        try (var outputStream = new ByteArrayOutputStream()) {
            blobClient.download(outputStream);
            return outputStream.toString();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }
    public PagedIterable<BlobItem> getBlobs() {
        try {
            BlobContainerClient blobContainerClient = createBlobContainerClient();
            return blobContainerClient.listBlobs();
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }
    public void uploadBlob(String blobName, String content) {
        try {
            var blobContainerClient = createBlobContainerClient();
            var blobClient = blobContainerClient.getBlobClient(blobName);

            var dataStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
            blobClient.upload(dataStream, content.length(), true);
            dataStream.close();
            System.out.println("SUBI EL ARCHIVO PARI");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
    public void removeBlob(String blobName) {
    }

    private BlobContainerClient createBlobContainerClient() {
        try {
            var blobServiceClient = new BlobServiceClientBuilder().endpoint(Configurations.ENDPOINT_DEV).buildClient();
            var blobContainerClient =  blobServiceClient.getBlobContainerClient(Configurations.BLOB_CONTAINER_NAME);

            if (!blobContainerClient.exists()) blobContainerClient.create();
            return blobContainerClient;
        } catch (Exception ex) {
            ExceptionHandler.TrackException(ex, null);
            throw ex;
        }
    }
}
