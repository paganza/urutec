package com.paganza.urutec.handlers;

import com.google.gson.Gson;
import com.paganza.urutec.services.CommunicationPlatformService;
import com.paganza.urutec.services.TelemetryService;
import java.util.HashMap;

public class ExceptionHandler {
    private static Gson gson = new Gson();
    private ExceptionHandler() { }

    public static void TrackException(Exception ex, Object object) {
        try {
            TelemetryService.getInstance().trackException(ex, object == null ? null : new HashMap<>() {{
                put("Object", gson.toJson(object));
            }}, null);
            new CommunicationPlatformService().Notify(ex.toString());
        } catch (Exception e) { }
        System.out.println(ex);
    }
}
