package com.paganza.urutec.handlers;

import com.paganza.urutec.console.Configurations;
import com.paganza.urutec.interfaces.IBlobStorageService;
import com.paganza.urutec.interfaces.ICCAExecutor;
import com.paganza.urutec.runners.BankTransactionsPusherRunner;
import mtbevsa.bd.TransaccionBancoAcreditadaCancelada;

public class PendingAccreditationBlobContainerHandler extends BankTransactionsPusherRunner {
    public PendingAccreditationBlobContainerHandler(IBlobStorageService blobStorageService, ICCAExecutor iccaExecutor) {
        super(blobStorageService, iccaExecutor, Configurations.PENDING_ACCREDITATION_BLOB_CONTAINER_PATH);
    }

    @Override
    protected Object pullTransactions() {
        var txString = this.blobStorageService.getBlob("asd");
        return this.gson.fromJson(txString, TransaccionBancoAcreditadaCancelada[].class);
    }
    @Override
    protected void processTransactions(Object transactions) {
        var txs = (TransaccionBancoAcreditadaCancelada[])transactions;
        for (var t:txs){
            t.setNumeroAutorizacion("1");
            t.setCodigoEstadoTransacAcrCan('A');
            t.setObservaciones("Cancelada porque no se encontró al usuario");
        }
    }

}
