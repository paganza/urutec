package com.paganza.urutec.handlers;

import com.paganza.urutec.console.Configurations;
import com.paganza.urutec.helpers.DateFormatterHelper;
import com.paganza.urutec.interfaces.ICCAExecutor;
import com.paganza.urutec.interfaces.ICCAService;
import com.paganza.urutec.runners.BankTransactionsPullerRunner;
import com.paganza.urutec.interfaces.IBlobStorageService;
import mtbevsa.clienteMTBevsa.libreria.LibreriaException;
import mtbevsa.comMtBevsa.MTConstantes;

import java.io.IOException;

public class PendingBlobContainerHandler extends BankTransactionsPullerRunner {
    public PendingBlobContainerHandler(IBlobStorageService blobStorageService, ICCAExecutor iccaExecutor) {
        super(blobStorageService, iccaExecutor, Configurations.PENDING_BLOB_CONTAINER_PATH);
    }

    @Override
    public Object pullTransactions(ICCAService ccaService) throws LibreriaException, IOException {
        return ccaService.novedades(MTConstantes.TRANSF_RECEPTOR,
                                    MTConstantes.TIPO_TR_LIQ_COMPENSADA,
                                    DateFormatterHelper.libraryDateFormat(DateFormatterHelper.dateAtStartOfDay()));
    }
}
