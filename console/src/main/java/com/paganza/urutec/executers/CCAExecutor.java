package com.paganza.urutec.executers;

import com.paganza.urutec.console.Configurations;
import com.paganza.urutec.handlers.ExceptionHandler;
import com.paganza.urutec.interfaces.ICCAExecutable;
import com.paganza.urutec.interfaces.ICCAExecutor;
import com.paganza.urutec.services.CCAService;
import mtbevsa.clienteMTBevsa.libreria.LibreriaException;

import java.util.LinkedList;
import java.util.Queue;

public class CCAExecutor implements ICCAExecutor {
    private static CCAExecutor executor;
    private final Queue<CCAService> ccaServices = new LinkedList<>();
    private final Queue<ICCAExecutable> executionQueue = new LinkedList<>();

    private CCAExecutor() throws LibreriaException {
        for(var i = 0; i < Configurations.THREAD_NUMBERS; i++){
            ccaServices.add(new CCAService());
        }
    }
    public synchronized static CCAExecutor getInstance() throws LibreriaException {
        if (executor == null)
            executor = new CCAExecutor();
        return executor;
    }

    public void addExecution(ICCAExecutable execution) {
        executionQueue.add(execution);
        if (ccaServices.size() > 0)
        {
            var service = ccaServices.remove();
            var thread = new Thread(() -> execute(service));
            thread.start();
        }
    }

    private void execute(CCAService ccaService) {
        var exec = this.executionQueue.poll();
        if (exec == null) return;
        try {
            exec.run(ccaService);
            execute(ccaService);
        } catch (LibreriaException l) {
            try {
                ccaService.enableConnexion();
                handleExecuteException(exec, ccaService, l);
            } catch (Exception e) {
                handleExecuteException(exec, ccaService, e);
            }
        } catch (Exception ex) {
            handleExecuteException(exec, ccaService, ex);
        }
    }
    private void handleExecuteException(ICCAExecutable method, CCAService ccaService, Exception ex){
        this.addExecution(method);
        this.ccaServices.add(ccaService);
        ExceptionHandler.TrackException(ex, null);
    }
}
