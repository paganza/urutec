package com.paganza.urutec.schedulers;

import com.paganza.urutec.console.Configurations;
import com.paganza.urutec.handlers.PendingAccreditationBlobContainerHandler;
import com.paganza.urutec.handlers.PendingBlobContainerHandler;
import com.paganza.urutec.interfaces.IBlobStorageService;
import com.paganza.urutec.interfaces.ICCAExecutor;

import java.util.concurrent.Executors;

public class BlobContainerScheduler {
    public static void pendingScheduler(IBlobStorageService blobStorageService, ICCAExecutor iccaExecutor){
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() ->
            (new PendingBlobContainerHandler(blobStorageService, iccaExecutor)).start(),
            Configurations.PENDING_BLOB_CONTAINER_INITIAL_DELAY,
            Configurations.PENDING_BLOB_CONTAINER_PERIOD,
            Configurations.SCHEDULER_TIME_UNIT);
    }

    public static void pendingAccreditationScheduler(IBlobStorageService blobStorageService, ICCAExecutor iccaExecutor){
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() ->
            (new PendingAccreditationBlobContainerHandler(blobStorageService, iccaExecutor)).start(),
            Configurations.PENDING_ACCREDITATION_BLOB_CONTAINER_INITIAL_DELAY,
            Configurations.PENDING_ACCREDITATION_BLOB_CONTAINER_PERIOD,
            Configurations.SCHEDULER_TIME_UNIT);
    }
}
