package com.paganza.urutec.dispatchers;

import com.paganza.urutec.helpers.Builders;
import com.paganza.urutec.interfaces.ICCAExecutable;
import com.paganza.urutec.interfaces.ICCAExecutor;
import com.paganza.urutec.interfaces.ICCAService;
import mtbevsa.clienteMTBevsa.libreria.LibreriaException;

import java.io.IOException;

public class TransferDispatcher {
    private final ICCAExecutor executor;

    public TransferDispatcher(ICCAExecutor executor) {
        this.executor = executor;
    }

    public void sendTransferDummy() {
       this.executor.addExecution(iccaService -> iccaService.sendTransactions(Builders.Transaccion("UYU", "250")));
    }
}
