package com.paganza.urutec.interfaces;

import mtbevsa.bd.TransaccionBanco;
import mtbevsa.bd.TransaccionBancoAcreditadaCancelada;
import mtbevsa.clienteMTBevsa.libreria.LibreriaException;
import mtbevsa.clienteMTBevsa.libreria.Transaccion;
import mtbevsa.clienteMTBevsa.mensajeria.Status;

import java.io.IOException;


public interface ICCAService {
        // Obtiene las transacciones de HOY. Se indica la hora a partir de la cual debe buscar en hhmmss
        TransaccionBanco[] getTransactionsByHour(final String fromHour) throws LibreriaException, IOException;

        // Obtiene las transacciones entre un rango de fechas. El formato de las fechas es AAAAMMDDhhmmss
        // Si se quiere obtener todas las transacciones en adelante, el parámetro de la segunda fecha deberá ser null
        TransaccionBanco[] getTransactionsByRangeDate(final String fromDate, final String toDate) throws LibreriaException, IOException;

        // Obtiene todas las transacciones banco liquidadas del día a partir de una hora
        TransaccionBanco[] consultarTransaccionesDestinoLiquidadas(final String fromHour) throws LibreriaException, IOException;

        TransaccionBancoAcreditadaCancelada[] novedades(final String orden, final char liquidationType, final String fromDate) throws LibreriaException, IOException;

        // Obtiene todas las transacciones pendientes de acreditación/cancelación del día a partir de una hora en particular (hhmmss)
        TransaccionBancoAcreditadaCancelada[] getTransactionsByHourToAccreditOrCancel(final String fromHour) throws LibreriaException, IOException;

        // Obtiene todas las transacciones acreditadas/canceladas liquidadas entre dos fechas,
        // donde la fecha inicial se comunica por parámetro y la fecha final es la fecha actual.
        TransaccionBancoAcreditadaCancelada[] getTransactionsAccreditedCancelatedLiquidadas(final String fromDate) throws LibreriaException, IOException;

        // Acredita transacciones
        // A cada transacción se le debe setear los siguientes campos:
        // - Id. Transacción banco ******** DE DONDE CORNO LO SACO?
        // - Número de autorización ******** DE DONDE CORNO LO SACO?
        // - Observaciones ******** DE DONDE CORNO LO SACO?
        Status accreditTransaction(final TransaccionBancoAcreditadaCancelada[] transactions) throws LibreriaException, IOException;

        // Cancela transacciones
        // A cada transacción se le debe setear los siguientes campos:
        // - Id. Transacción banco ******** DE DONDE CORNO LO SACO?
        // - Código motivo cancelación ******** DE DONDE CORNO LO SACO?
        // - Observaciones ******** DE DONDE CORNO LO SACO?
        Status cancelTransaction(final TransaccionBancoAcreditadaCancelada[] transactions) throws LibreriaException, IOException;

        Status sendTransactions(final Transaccion tx) throws LibreriaException, IOException;
}
