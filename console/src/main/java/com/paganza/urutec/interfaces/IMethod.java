package com.paganza.urutec.interfaces;

import mtbevsa.clienteMTBevsa.libreria.Libreria;

public interface IMethod {
    void run(Libreria library);
}
