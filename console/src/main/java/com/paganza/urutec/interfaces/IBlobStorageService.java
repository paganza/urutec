package com.paganza.urutec.interfaces;

import com.azure.core.http.rest.PagedIterable;
import com.azure.storage.blob.models.BlobItem;

public interface IBlobStorageService {
    void uploadBlob(final String blobName, final String content);
    void removeBlob(final String blobName);
    String getBlob(final String blobName);
    PagedIterable<BlobItem> getBlobs();
}
