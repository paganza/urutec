package com.paganza.urutec.interfaces;

import mtbevsa.clienteMTBevsa.libreria.LibreriaException;

import java.io.IOException;

public interface ICCAExecutable<T extends ICCAService, ICCAServiceConnexion> {
    void run(T t) throws LibreriaException, IOException;
}
