package com.paganza.urutec.interfaces;

import mtbevsa.clienteMTBevsa.libreria.LibreriaException;

public interface ICCAServiceConnexion {
    void enableConnexion() throws LibreriaException;
}
