package com.paganza.urutec.interfaces;

import java.util.Map;

public interface ITelemetryService {
    void trackEvent(final String eventName, final Map<String, String> properties);
    void trackException(final Exception exception, final Map<String, String> properties);
    void trackMetric(final String name, final double value);
}
